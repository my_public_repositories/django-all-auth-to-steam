from __future__ import print_function
import sib_api_v3_sdk
from sib_api_v3_sdk.rest import ApiException
from pprint import pprint

import environ  # Initialise environment variables

env = environ.Env()
environ.Env.read_env()

configuration = sib_api_v3_sdk.Configuration()
configuration.api_key["api-key"] = env("API_BREVO_KEY_VALUE")

api_instance = sib_api_v3_sdk.TransactionalEmailsApi(sib_api_v3_sdk.ApiClient(configuration))
subject = "Test BREVO"
html_content = "<html><body><h1>This is my first transactional email </h1></body></html>"
sender = {"name": "John Doe", "email": "admin@seer-cs.com"}
to = [{"email": "admin@seer-cs.com", "name": "Jane Doe"}]
reply_to = {"email": "admin@seer-cs.com", "name": "John Doe"}
headers = {"Some-Custom-Name": "unique-id-1234"}
params = {"parameter": "My param value", "subject": "New Subject"}
send_smtp_email = sib_api_v3_sdk.SendSmtpEmail(
    to=to, reply_to=reply_to, headers=headers, html_content=html_content, sender=sender, subject=subject
)

try:
    api_response = api_instance.send_transac_email(send_smtp_email)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SMTPApi->send_transac_email: %s\n" % e)
