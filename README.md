# django-all-auth-to-steam

test allauth package in django project to implement steam connection

starter based on this tutorial: https://testdriven.io/blog/django-social-auth/ 

librairie bug but it is fixed: https://github.com/pennersr/django-allauth/issues/3404

steam pb explain here: https://github.com/pallets-eco/flask-openid/issues/48 

step: 
- poetry install
- poetry shell
- python manage.py migrate
- python manage.py createsuperuser
- python manage.py runserver
- in admin: http://127.0.0.1:8000/admin create your steam social app with client Id and key as api key : https://steamcommunity.com/dev/apikey
- Run the app. You should now be able to log in via Steam.

user info exemple:
https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=STEAM_API_KEY&steamids=76561198426829752

# in a second time I add email auth 

https://medium.com/powered-by-django/add-social-authentication-django-allauth-google-oauth-example-d8d69a603356 
https://www.codesnail.com/django-allauth-email-authentication-tutorial/
https://medium.com/@kennethjiang/email-social-account-login-in-django-e043b6c27686
https://copyprogramming.com/howto/how-do-i-get-django-allauth-to-send-html-emails
https://docs.allauth.org/en/latest/account/configuration.html

# mail / brevo

https://anymail.dev/en/stable/esps/brevo/ 
https://medium.com/@sangeeth123sj/wanna-send-emails-from-your-python-backend-lets-implement-this-feature-using-brevo-f507c87a7f52
